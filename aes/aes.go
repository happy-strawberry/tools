package aes

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"io"
	"strings"
)

// Decrypt 解密
func Decrypt(data string, keyStr string) ([]byte, error) {

	key := []byte(keyStr)
	aesData := strings.Split(data, ":")

	ciphertext0, err := decodeHex(aesData[0])
	if err != nil {
		return nil, err
	}

	ciphertext1, err := decodeHex(aesData[1])
	if err != nil {
		return nil, err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	blockMode := cipher.NewCBCDecrypter(block, ciphertext0)
	crypt := make([]byte, len(ciphertext1))
	blockMode.CryptBlocks(crypt, ciphertext1)

	return pkcs7UnPadding(crypt)
}

func decodeHex(data string) ([]byte, error) {
	return hex.DecodeString(data)
}

func pkcs7UnPadding(data []byte) ([]byte, error) {
	length := len(data)
	if length == 0 {
		return nil, errors.New("pkcs7UnPadding error！")
	}

	unPadding := int(data[length-1])
	return data[:(length - unPadding)], nil
}

func Encrypt(data []byte, keyStr string) (string, error) {
	key := []byte(keyStr)
	//创建加密实例
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	//判断加密快的大小
	blockSize := block.BlockSize()
	//填充
	encryptBytes := pkcs7Padding(data, blockSize)
	//初始化加密数据接收切片
	crypted := make([]byte, len(encryptBytes))
	//使用cbc加密模式
	blockMode := cipher.NewCBCEncrypter(block, key[:blockSize])
	//执行加密
	blockMode.CryptBlocks(crypted, encryptBytes)

	return hex.EncodeToString(key[:blockSize]) + ":" + hex.EncodeToString(crypted), nil
}

// pkcs7Padding 填充
func pkcs7Padding(data []byte, blockSize int) []byte {
	//判断缺少几位长度。最少1，最多 blockSize
	padding := blockSize - len(data)%blockSize
	//补足位数。把切片[]byte{byte(padding)}复制padding个
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, padText...)
}

// EncryptGCM256 AES-126-GCM
func EncryptGCM256(keyv, s string) string {
	key, _ := hex.DecodeString(keyv)
	//key := hexStringToByte(keyv)
	plaintext := []byte(s)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	// Never use more than 2^32 random nonces with a given key because of the risk of a repeat.
	nonce := make([]byte, 12)
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	//nonce, _ = hex.DecodeString("FA9F93725406FD0E4F621B01")
	//iv := []byte("123456789012")
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	ciphertext := aesgcm.Seal(nil, nonce, plaintext, nil)

	r := append(nonce, ciphertext...)
	//r, _ := hex.DecodeString(fmt.Sprintf("%x", ciphertext))
	return base64.StdEncoding.EncodeToString(r)
}

func GenSHA256HashCode(message []byte) string {
	//方法一：
	//创建一个基于SHA256算法的hash.Hash接口的对象
	hash := sha256.New()
	//输入数据
	hash.Write(message)
	//计算哈希值
	bytes := hash.Sum(nil)
	//将字符串编码为16进制格式,返回字符串
	hashCode := hex.EncodeToString(bytes)
	//返回哈希值
	return hashCode
}
