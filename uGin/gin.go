package uGin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"net/http"
)

type Gin struct {
	Ctx *gin.Context
}

// BaseRsp
// @Description: 通用返回 json数据 {"code": code, "data": data, "msg": msg}
type BaseRsp struct {
	Code int         `json:"code" doc:"错误码"`
	Msg  string      `json:"msg" doc:"错误信息"`
	Data interface{} `json:"data" doc:"数据包"`
}

// RequestParsing
// @Description: 请求解析
func (ug *Gin) RequestParsing(obj interface{}) bool {
	if err := ug.Ctx.ShouldBindBodyWith(&obj, binding.JSON); err != nil {
		fmt.Println("[uGin] param bind err : ", err)
		ug.UnprocessableEntity(http.StatusUnprocessableEntity, "请求参数解析错误!")
		return false
	}
	return true
}

// ResponseData
// @Description: 响应数据
func (ug *Gin) ResponseData(httpStatus int, code int, data interface{}, msg string) {
	ug.Ctx.JSON(httpStatus, gin.H{"code": code, "data": data, "msg": msg})
}

func (ug *Gin) Ok(code int, data interface{}, msg string) {
	ug.ResponseData(http.StatusOK, code, data, msg)
}

func (ug *Gin) OkSuccess(data interface{}, msg string) {
	ug.Ok(0, data, msg)
}

func (ug *Gin) BadRequest(code int, msg string) {
	ug.ResponseData(http.StatusBadRequest, code, nil, msg)
}

func (ug *Gin) InternalServerError(code int, msg string) {
	ug.ResponseData(http.StatusInternalServerError, code, nil, msg)
}

func (ug *Gin) UnprocessableEntity(code int, msg string) {
	ug.ResponseData(http.StatusUnprocessableEntity, code, nil, msg)
}
