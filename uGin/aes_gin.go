package uGin

import (
	"encoding/json"
	"fmt"
	"gitee.com/happy-strawberry/tools/aes"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"net/http"
)

type AesGin struct {
	Ctx *gin.Context
}

// BaseAesRsp
// @Description: 通用返回 json数据 {"code": code, "data": data, "msg": msg}
type BaseAesRsp struct {
	Code int         `json:"code" doc:"错误码"`
	Msg  string      `json:"msg" doc:"错误信息"`
	Data interface{} `json:"data" doc:"数据包"`
}

// EncryptRes
// @Description: 加密通用请求 json数据 {"data": EncryptResData}
type EncryptRes struct {
	Data string `from:"data" json:"data"`
}

// RequestParsingAES
// @Description: 请求数据-解密
func (ug *AesGin) RequestParsingAES(obj interface{}, key string) bool {
	// 捕获恐慌
	defer func() {
		if v := recover(); v != nil {
			fmt.Println("解析加密请求数据恐慌:", v)
			return
		}
	}()
	// 获取请求主体参数
	var param EncryptRes
	if err := ug.Ctx.ShouldBindBodyWith(&param, binding.JSON); err != nil {
		fmt.Println("请求参数解析错误:", err.Error())
		ug.UnprocessableEntity(http.StatusUnprocessableEntity, "请求参数解析错误!", key)
		return false
	}

	// 解析aes加密串
	realBody, aesDecryptErr := aes.Decrypt(param.Data, key)
	if aesDecryptErr != nil {
		fmt.Println("请求参数解密失败:", aesDecryptErr.Error())
		ug.UnprocessableEntity(0, "请求参数解析错误!", key)
		return false
	}

	// 将json字符串解析到响应结构体
	if unmarshalErr := json.Unmarshal(realBody, &obj); unmarshalErr != nil {
		fmt.Println("请求参数json.Unmarshal出错:", unmarshalErr.Error())
		ug.UnprocessableEntity(0, "请求参数解析错误!", key)
		return false
	}
	return true
}

// ResponseDataAES
// @Description: 响应数据-加密
func (ug *AesGin) ResponseDataAES(httpStatus int, code int, data interface{}, msg, key string) {
	// 将响应体转为字符串
	aesByte, marshalErr := json.Marshal(gin.H{"code": code, "data": data, "msg": msg})
	if marshalErr != nil {
		fmt.Println("响应参数json.marshal出错:", marshalErr.Error())
		ug.UnprocessableEntity(500, "响应参数解析错误!", key)
		return
	}
	// 加密
	aesStr, aesEncryptErr := aes.Encrypt(aesByte, key)
	if aesEncryptErr != nil {
		fmt.Println("响应主体加密出错:", aesEncryptErr.Error())
		ug.UnprocessableEntity(500, "响应参数加密错误!", key)
		return
	}
	ug.Ctx.JSON(httpStatus, gin.H{"data": aesStr})
}

func (ug *AesGin) Ok(code int, data interface{}, msg, key string) {
	ug.ResponseDataAES(http.StatusOK, code, data, msg, key)
}

func (ug *AesGin) OkSuccess(data interface{}, msg, key string) {
	ug.Ok(0, data, msg, key)
}

func (ug *AesGin) BadRequest(code int, msg, key string) {
	ug.ResponseDataAES(http.StatusBadRequest, code, nil, msg, key)
}

func (ug *AesGin) InternalServerError(code int, msg, key string) {
	ug.ResponseDataAES(http.StatusInternalServerError, code, nil, msg, key)
}

func (ug *AesGin) UnprocessableEntity(code int, msg, key string) {
	ug.ResponseDataAES(http.StatusUnprocessableEntity, code, nil, msg, key)
}
