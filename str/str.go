package str

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"math/rand"
	"regexp"
	"time"
	"unicode"
)

func ToMD5(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

// Rand 生成随机大写字母加数字字符串
func Rand(len int) string {
	var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, len)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := range b {
		b[i] = letters[r.Intn(62)]
	}
	return string(b)
}

// RandUpperAndNumber 生成一个由大写字母和数字组成的随机字符串。
func RandUpperAndNumber(len int) string {
	var letters = []rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, len)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := range b {
		b[i] = letters[r.Intn(32)]
	}
	return string(b)
}

// RandUpper 生成一个由大写字母组成的随机字符串。
func RandUpper(len int) string {
	var letters = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, len)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := range b {
		b[i] = letters[r.Intn(24)]
	}
	return string(b)
}

// IsChinese 检查字符串中是否包含中文
func IsChinese(str string) bool {
	for _, r := range str {
		if unicode.Is(unicode.Scripts["Han"], r) || (regexp.MustCompile("[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b]").MatchString(string(r))) {
			return true
		}
	}
	return false
}

// CheckPassword 通用的密码格式验证
func CheckPassword(ps string) error {
	if len(ps) < 8 {
		return fmt.Errorf("密码长度不能小于8位")
	}
	num := `[0-9]{1}`
	aZ := `[a-z]{1}`
	AZ := `[A-Z]{1}`
	if b, err := regexp.MatchString(num, ps); !b || err != nil {
		return fmt.Errorf("至少包含一个数字")
	}
	if b, err := regexp.MatchString(aZ, ps); !b || err != nil {
		return fmt.Errorf("至少包含一个小写字母")
	}
	if b, err := regexp.MatchString(AZ, ps); !b || err != nil {
		return fmt.Errorf("至少包含一个大写字母")
	}
	if IsChinese(ps) {
		return fmt.Errorf("不能包含中文字符")
	}
	return nil
}
